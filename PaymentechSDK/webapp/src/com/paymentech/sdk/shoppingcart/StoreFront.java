package com.paymentech.sdk.shoppingcart;

import java.text.NumberFormat;
import java.util.Locale;

import java.util.Properties; 
/**
 * Please note that this code is for documentation purposes only and is 
 * specifically written in a simplistic way, in order to better represent and 
 * convey specific concepts in the use of the Orbital SDK.  
 * Best practices in error handling and control flow for java coding, 
 * have often been ignored in favor of providing code that clearly represents 
 * a specific concept.  
 * Due to the best practices compromises we have made in our sample code, 
 * we do not recommend using this code in a production environment without 
 * rigorous improvements to error handling and overall architecture.
 */
public class StoreFront
{
  public static String STRINGID = "Store";
  public StoreFront()
  {
    setLocale();
    try {
      setTable();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * This info would ideally come from a table
   */
  StringBuffer bookTable;
  String[] bookTitles = {"Orbital SDK - Java for Dummies","Orbital SDK - C++ for Dummies","Spectrum SDK - Java for Dummies","Spectrum SDK - C++ for Dummies","Spectrum SDK - Perl for Dummies","Spectrum SDK - COM for Dummies","Spectrum SDK - .Net for Dummies"};
  String[] bookPrices = {"5.80","4.80","7.99","12.95","9.99","15.50","5.95"}  ;


  public String getTitle(String id) {
    int i=0;
    try {
      i = Integer.parseInt(id);
    }
    catch (Exception ex) {
      return null;
    }
    return bookTitles[i];
  }

  public String getPrice(String id) {
    int i=0;
    try {
      i = Integer.parseInt(id);
    }
    catch (Exception ex) { }
    return bookPrices[i];
  }

  /**
   * sets up the bookTitlesTable array with rows from the book table
   */
  public void setTable() {
    int count = ShoppingCart.getPageCount();
    bookTable = new StringBuffer();
    NumberFormat nf =  NumberFormat.getCurrencyInstance();
    for (int i = 0; i < bookTitles.length; i++) {
       double price = string2Double(bookPrices[i]);
       bookTable.append("<TR>" +
       "<TD><A HREF=Cart.jsp?itemID=" +
       i + "&count=" + count + ">" +
       bookTitles[i] + "</A></TD>" +
       "<TD align=\"RIGHT\">" +  nf.format(price) +  "</TD>" +
       "</TR>\n");
    }
  }


  /**
   * returns an html table of the book titles from the book table
   * @return String
   */
  public String getTable() {
    return bookTable.toString();
  }

  private double string2Double(String s) {
    double d;
    try {
       d = Double.parseDouble(s);
    }
    catch (NumberFormatException ex) {
      d = 0.0;
    }
    return d;
  }

  private void setLocale() {
    if (Locale.getDefault().getCountry() == "") {
      try {
        Locale.setDefault(Locale.US);
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
  }
}
