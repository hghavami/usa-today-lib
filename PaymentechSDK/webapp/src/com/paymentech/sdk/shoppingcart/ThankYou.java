package com.paymentech.sdk.shoppingcart;
import javax.servlet.http.HttpServletRequest;
/**
 * Please note that this code is for documentation purposes only and is 
 * specifically written in a simplistic way, in order to better represent and 
 * convey specific concepts in the use of the Orbital SDK.  
 * Best practices in error handling and control flow for java coding, 
 * have often been ignored in favor of providing code that clearly represents 
 * a specific concept.  
 * Due to the best practices compromises we have made in our sample code, 
 * we do not recommend using this code in a production environment without 
 * rigorous improvements to error handling and overall architecture.
 */
public class ThankYou 
{
  public String processOrder(ShoppingCart cart , Purchase regis, StoreFront store,HttpServletRequest currrequest)
  {
	  		SimpleAuthInternet conn = new SimpleAuthInternet ();
		  return conn.processOrder(cart, regis, store, currrequest);
}
}

