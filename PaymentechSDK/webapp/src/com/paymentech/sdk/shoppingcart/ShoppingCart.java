package com.paymentech.sdk.shoppingcart;
import java.util.*;
import java.text.NumberFormat;
import javax.servlet.http.HttpServletRequest;
/**
 * Please note that this code is for documentation purposes only and is 
 * specifically written in a simplistic way, in order to better represent and 
 * convey specific concepts in the use of the Orbital SDK.  
 * Best practices in error handling and control flow for java coding, 
 * have often been ignored in favor of providing code that clearly represents 
 * a specific concept.  
 * Due to the best practices compromises we have made in our sample code, 
 * we do not recommend using this code in a production environment without 
 * rigorous improvements to error handling and overall architecture.
 */
public class  ShoppingCart
{
  private static int pageCount = 0;
  private boolean pageRefresh = false;
  private ArrayList items;

  public ShoppingCart()
  {
    init();
  }

  public void init()
  {
    items = new ArrayList();
  }

  static int getPageCount()
  {
    return pageCount++;
  }
  public void checkPageCount(int storeCount)
  {
    if (storeCount >= (getPageCount() - 1))
    {
      setPageRefresh(false);
    } else
    {
      setPageRefresh(true);
    }
  }
  private void setPageRefresh(boolean isRefresh)
  {
    pageRefresh = isRefresh;
  }

  private boolean getPageRefresh()
  {
    return pageRefresh;
  }

  public ArrayList getItems()
  {
    return items;
  }

  public String getID(HttpServletRequest request)
  {
     String id;
     if (request.getMethod().equals("POST"))
     {
       id = null;
       String remove = request.getParameter("Remove");
       int size = items.size();
       if (remove != null) {
         for (int i = size; i > 0; i--)
         {
           String removeBook = request.getParameter("REMOVE_BOOK"+(i-1));
           if (removeBook != null) {
             items.remove(i-1);
           }
         }
       }
     }
     else if (!getPageRefresh())
     {
       id = request.getParameter("itemID");
     }
     else {
       id = null;
     }
     return id;
  }

  public void addItem(String id) {
     items.add(id.intern());
  }

  public int getSize() {
    int size = 0;
    if (items != null)
      return size = items.size();
    return size;
  }

  public String getTable(StoreFront list)
  {
    StringBuffer table = new StringBuffer();
    NumberFormat nf =  NumberFormat.getCurrencyInstance();
    for (int i = 0; i < items.size(); i++)
    {
       String item = (String) items.get(i);
       double price = Double.parseDouble(list.getPrice(item));
       table.append("<TR>" +
       "<TD ALIGN=\"CENTER\"><INPUT TYPE=CHECKBOX NAME=REMOVE_BOOK" + i +" > </TD>"+
       "<TD>" + list.getTitle(item) + "</TD>" +
       "<TD ALIGN=\"RIGHT\">" + nf.format(price) + "</TD></TR>\n");
    }
    return table.toString();
  }

  public String getDispTotalPrice(StoreFront list)
  {
    double price = 0;
    for (int i = 0; i < items.size(); i++)
    {
       String item = (String) items.get(i);
       price += Double.parseDouble(list.getPrice(item));
    }
    NumberFormat nf =  NumberFormat.getCurrencyInstance();
    return nf.format(price);
  }

  public double getTotalPrice(StoreFront list)
 {
   double price = 0;
   for (int i = 0; i < items.size(); i++)
   {
      String item = (String) items.get(i);
      price += Double.parseDouble(list.getPrice(item));
   }
    return price;
 }

}
