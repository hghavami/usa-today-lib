package com.paymentech.sdk.shoppingcart;
import javax.servlet.http.HttpServletRequest;
import com.paymentech.orbital.sdk.configurator.Configurator;
import com.paymentech.orbital.sdk.configurator.ConfiguratorIF;
import com.paymentech.orbital.sdk.interfaces.RequestIF;
import com.paymentech.orbital.sdk.interfaces.ResponseIF;
import com.paymentech.orbital.sdk.interfaces.TransactionProcessorIF;
import com.paymentech.orbital.sdk.request.FieldNotFoundException;
import com.paymentech.orbital.sdk.request.Request;
import com.paymentech.orbital.sdk.transactionProcessor.TransactionException;
import com.paymentech.orbital.sdk.transactionProcessor.TransactionProcessor;
import com.paymentech.orbital.sdk.util.exceptions.InitializationException;
/**
 * Please note that this code is for documentation purposes only and is 
 * specifically written in a simplistic way, in order to better represent and 
 * convey specific concepts in the use of the Orbital SDK.  
 * Best practices in error handling and control flow for java coding, 
 * have often been ignored in favor of providing code that clearly represents 
 * a specific concept.  
 * Due to the best practices compromises we have made in our sample code, 
 * we do not recommend using this code in a production environment without 
 * rigorous improvements to error handling and overall architecture.
 */
public class SimpleAuthInternet 
{
	public SimpleAuthInternet()
	{
	}

	public String processOrder(ShoppingCart cart , Purchase regis, StoreFront store,HttpServletRequest currrequest)
	{
		 try {
			 	ConfiguratorIF configurator = null;
			 	configurator = Configurator.getInstance();
	        } 
		 	catch (InitializationException ie) 
		 	{
	            return "Exception while initializing the SDK " + ie.toString();
	        }
	        //Create a request object
	        //The request object uses the XML templates along with data we provide
	        //to generate a String representation of the xml request
	        RequestIF request = null;
	        try {
	            //Tell the request object which template to use (see RequestIF.java)
	            request = new Request(RequestIF.NEW_ORDER_TRANSACTION);
	            //If there were no errors preparing the template, we can now specify the data
	            //Basic Auth Fields
	            request.setFieldValue("OrbitalConnectionUsername", currrequest.getParameter("UserName"));
	            request.setFieldValue("OrbitalConnectionPassword", currrequest.getParameter("Password"));
	            request.setFieldValue("IndustryType", "EC");
	            request.setFieldValue("MessageType", "A");
	            request.setFieldValue("MerchantID", currrequest.getParameter("MerchantID"));
	            request.setFieldValue("BIN", currrequest.getParameter("BIN"));
	            request.setFieldValue("OrderID", currrequest.getParameter("OrderID"));
	            request.setFieldValue("AccountNum", currrequest.getParameter("ccNumber"));
	            request.setFieldValue("Amount", regis.getFinalPrice(store,cart));
	            request.setFieldValue("Exp", currrequest.getParameter("ccDate"));
	            // AVS Information
	            request.setFieldValue("AVSname", currrequest.getParameter("ccName"));
	            request.setFieldValue("AVSaddress1", currrequest.getParameter("address"));
	            request.setFieldValue("AVScity", currrequest.getParameter("city"));
	            request.setFieldValue("AVSstate", currrequest.getParameter("state"));
	            request.setFieldValue("AVSzip", currrequest.getParameter("zip"));
	            // Additional Information
	            request.setFieldValue("Comments", "This is Java SDK");
	            request.setFieldValue("ShippingRef", "FEDEX WB12345678 Pri 1");
		    System.out.println(request.getXML());
	        }
	        catch (InitializationException ie) 
	        {
	            return "Unable to initialize request object " + ie.toString();
	        } 
	        catch (FieldNotFoundException fnfe) 
	        {
	            return "Unable to find XML field in template " + fnfe.toString();
	        } 
	        catch (Exception e) 
	        {
	        	return "Unable to initialize request object " + e.toString();
			}
	        
	        //Create a Transaction Processor
	        //The Transaction Processor acquires and releases resources and executes transactions.
	        //It configures a pool of protocol engines, then uses the pool to execute transactions.
	        TransactionProcessorIF tp = null;
	        try {
	            tp = new TransactionProcessor();
	        } 
	        catch (InitializationException iex) 
	        {
	            return "TransactionProcessor failed to initialize " + iex.toString();
	        }
	        //Process the transaction
	        //Pass in the request object (created above), and receive a response object.
	        //If the resources required by the Transaction Processor have been exhausted,
	        //this code will block until the resources become available.
	        //The "TransactionProcessor.poolSize" configuration property specifies how many resources
	        //will be available.  The TransactionProcessor acts as a governor, only allowing
	        //up to "poolSize" transactions outstanding at any point in time.
	        //As transactions are completed, their resources are placed back in the pool.
	        ResponseIF resp = null;
	        try 
	        {
	            resp = tp.process(request);
	            if ( resp.isGood())
	            	return "Your transaction ref. no is " + resp.getTxRefNum();
	            else
	            	return "Error while processing " + resp.getMessage();
	        } 
	        catch (TransactionException tex) 
	        {
	            return "Transaction failed, including retries and failover " + tex.toString();
	        }
	}
}
