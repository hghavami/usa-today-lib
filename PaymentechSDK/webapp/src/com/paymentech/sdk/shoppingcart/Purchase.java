
package com.paymentech.sdk.shoppingcart;

import java.util.*;
import java.lang.*;

import java.text.NumberFormat;
import javax.servlet.http.*;
/**
 * Please note that this code is for documentation purposes only and is 
 * specifically written in a simplistic way, in order to better represent and 
 * convey specific concepts in the use of the Orbital SDK.  
 * Best practices in error handling and control flow for java coding, 
 * have often been ignored in favor of providing code that clearly represents 
 * a specific concept.  
 * Due to the best practices compromises we have made in our sample code, 
 * we do not recommend using this code in a production environment without 
 * rigorous improvements to error handling and overall architecture.
 */
public class  Purchase {

  static int cartCounter = 1;
  static double price = 11.65;

  public Purchase()
  {
   };


  public String getDispShippingCost()
  {
    NumberFormat nf =  NumberFormat.getCurrencyInstance();
    return nf.format(price);
  }

  public double getShippingCost()
   {
      return price * 100;
  }


  public String getFinalPrice( StoreFront list,ShoppingCart cart)
  {
      return Long.toString(Math.round( getShippingCost() + ( cart.getTotalPrice(list)) * 100 ));
   }




  public String getDispFinalPrice(StoreFront list, ShoppingCart cart) {
    NumberFormat nf = NumberFormat.getCurrencyInstance();
    double total = 0;
    try {
      String shipping = getDispShippingCost();
      String books = cart.getDispTotalPrice(list);

      Number s = nf.parse(shipping);
      Number b = nf.parse(books);

      total = s.doubleValue() + b.doubleValue();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return nf.format(total);
  }





  public void removeBooks(HttpServletRequest request, ShoppingCart cart) {
    ArrayList items = cart.getItems();
    if (request.getMethod().equals("POST")) {
      String remove = request.getParameter("Remove");
      int size = items.size();
      // if the Remove Books button was hit
      if (remove != null) {
        for (int i = size; i > 0; i--) {
          String removeBook = request.getParameter("REMOVE_BOOK"+(i-1));
          if (removeBook != null) {
            items.remove(i-1);
          }
        }
      }
    }
  }

}
