Three files are included in the bundle.  

###Windows and Unix Users###
root2028.pem 
------------
New Verisign certificate with a 2028 expiration date.   




###WINDOWS USERS###
ImportCert.bat 
--------------
Runs with no command line parameters or accepts custom settings which are documented in the .bat file header.  Imports a certificate file to a specified keystore or, by default, installs a certificate file in the keystore located in the Java install directory (using JAVA_HOME environment variable).

ViewCACerts.bat  
---------------
Runs with no command line parameters or accepts custom settings which are documented in the .bat file header.  Dumps the contents of a keystore to the screen and to a file located in the same directory as the keystore.  The default behavior is to dump the contents of the keystore associated with the Java install directory (using JAVA_HOME environment variable).




###UNIX USERS###
ImportCert.sh 
-------------
Runs with no command line parameters or accepts custom settings which are documented in the .sh file header.  Imports a certificate file to a specified keystore or, by default, installs a certificate file in the keystore located in the Java install directory (using JAVA_HOME environment variable).

ViewCACerts.sh
--------------
Runs with no command line parameters or accepts custom settings which are documented in the .sh file header.  Dumps the contents of a keystore to the screen and to a file located in the same directory as the keystore.  The default behavior is to dump the contents of the keystore associated with the Java install directory (using JAVA_HOME environment variable).

