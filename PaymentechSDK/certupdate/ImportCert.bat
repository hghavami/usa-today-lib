@echo off
@rem ***************************************************************
@rem MSDOS script to import certificate file to keystore
@rem
@rem Usage
@rem
@rem This bat file takes 3 parameters
@rem %1 = certificate file
@rem %2 = certificate password (Enter NONE if no password is needed)
@rem %3 = keystore 
@rem %4 = keystore password
@rem %5 = certificate alias 
@rem
@rem If no parameters are entered then defaults will be used
@rem %1 = root2028.pem (in same directory)
@rem %2 = (no password needed)
@rem %3 = %JAVA_HOME%\jre\lib\security\cacerts
@rem %4 = changeit
@rem %5 = root2028
@rem
@rem Example Usage
@rem ImportCert root2028.pem somepassword %JAVA_HOME%\jre\lib\security\cacerts changeit root2028
@rem 
@rem ***************************************************************

set certfile=%1
set certpassword=%2
set keystore=%3
set keystorepassword=%4
set alias=%5

@rem make sure all the parameters were entered
if "%1"=="" goto setDefaults
if "%2"=="" goto setDefaults
if "%3"=="" goto setDefaults
if "%4"=="" goto setDefaults
if "%5"=="" goto setDefaults
goto copyCert

:setDefaults
echo ******* Setting Defaults ******* 
set certfile=root2028.pem
set certpassword=
set keystore=%JAVA_HOME%\jre\lib\security\cacerts
set keystorepassword=changeit
set alias=root2028

:copyCert
if not "%certfile%"=="root2028.pem" goto skipcopy
echo ******* Copying certificate file to %JAVA_HOME%\jre\lib\security *******
copy %certfile% %JAVA_HOME%\jre\lib\security
set certfile=%JAVA_HOME%\jre\lib\security\%certfile%

:skipcopy
if not "%certpassword%"=="NONE" goto skippassword

:skippassword

if "%certpassword%"=="" goto runwopassword

call %JAVA_HOME%\bin\keytool -import -v -alias %alias% -file %certfile% -keypass %certpassword% -keystore %keystore% -storepass %keystorepassword%
goto done

:runwopassword
call %JAVA_HOME%\bin\keytool -import -v -alias %alias% -file %certfile% -keystore %keystore% -storepass %keystorepassword%

:done