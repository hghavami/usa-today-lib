@echo off
@rem ***************************************************************
@rem MSDOS script to print the Java keystore entries
@rem
@rem Usage
@rem
@rem This bat file takes 3 parameters
@rem %1 = cacerts file (relative to to Java install directory)
@rem %2 = keystore password
@rem
@rem If no parameters are entered then defaults will be used
@rem %1 = %JAVA_HOME%\jre\lib\security\cacerts
@rem %2 = changeit
@rem
@rem Example Usage
@rem ViewCACerts %JAVA_HOME%\jre\lib\security\cacerts changeit
@rem 
@rem ***************************************************************

set cacerts=%2
set password=%3

@rem make sure all the parameters were entered
if "%1"=="" goto setDefaults
if "%2"=="" goto setDefaults
goto run

:setDefaults
echo ******* Setting Defaults ******* 
set cacerts=%JAVA_HOME%\jre\lib\security\cacerts
set password=changeit

:run

call %JAVA_HOME%\bin\keytool -list -v -keystore %cacerts% -storepass %password%
echo ******* Dumping output to cacerts.txt in current directory *******
call %JAVA_HOME%\bin\keytool -list -v -keystore %cacerts% -storepass %password% > cacert.txt

:done