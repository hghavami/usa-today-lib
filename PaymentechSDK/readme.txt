# PaymentechSDK_6.8.0
**********************************************
Orbital Java Software Developers Kit (SDK)
File: readme.txt
**********************************************

Copyright (c) 2004 Paymentech Inc. All Rights Reserved.

**************************************
Installation instructions
**************************************

Windows Systems
Unzip PaymentechSDK.zip at the directory location where you wish the SDK to reside.
If your zip software requires, be sure select "Use folder names" so the directory structure is preserved.

UNIX Systems
Use gunzip to uncompress the tar.gz file, then use tar xvf to explode the resulting tar file.

For detailed instructions for configuring and using the Orbital Java SDK, please review the Developers' Guide, located in the doc directory of your installation, 