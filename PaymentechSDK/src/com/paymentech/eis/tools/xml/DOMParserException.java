/* DOMParserException.java - Defines the exception class which indicates 
 * that something went wrong trying to parse an xml document.
 *
 * (C)opyright 2007, Chase Paymentech Solutions, LLC. All rights reserved
 * 
 * The copyright notice above does not evidence any actual or intended
 * publication of such source code.
 *
 * 
 *
 * $Header: /Data/CVSNT_repository/CVSRepo_032010/USA TODAY LIB/PaymentechSDK/src/com/paymentech/eis/tools/xml/DOMParserException.java,v 1.2 2013/04/03 20:38:34 hghavami Exp $
 *
 * Written by:
 *		$Author: hghavami $		$Revision: 1.2 $	$Date: 2013/04/03 20:38:34 $	$State: Exp $
 *
 * $Locker$
 * $Source: /Data/CVSNT_repository/CVSRepo_032010/USA TODAY LIB/PaymentechSDK/src/com/paymentech/eis/tools/xml/DOMParserException.java,v $
 *
 * Revision history:
 *
 * $Log: DOMParserException.java,v $
 * Revision 1.2  2013/04/03 20:38:34  hghavami
 * CC Orbital changes.
 *
 * Revision 1.1.4.1  2013/03/29 16:03:04  hghavami
 * Chase Orbital CC changes.
 *
 * Revision 1.1.2.1  2013/03/26 19:31:58  hghavami
 * Initial Commit.
 *
// 
//    Rev 1.1   Feb 07 2007 08:03:16   byounie
// Changed copyright
// 
//    Rev 1.0   Sep 05 2006 17:35:40   bkisiel
// Initial revision.
 * 
 * 1     12/09/03 8:24a Sayers
 * 
 * 2     4/02/01 4:28p Jpalmiero
 * Changed package from com.pt to com.paymentech
*/

// Package declaration
package com.paymentech.eis.tools.xml;

/**
 * DOMParserException - Indicates that something went wrong trying to parse
 * an xml document.
 *
 * @author		jpalmiero
 * @version		$Revision: 1.2 $
*/
public class DOMParserException extends Exception
{
	/**
	 * Constructs the exception with the human-readable error message. 
	 *
	 * @params	msg		The human-readable error message.
	*/
	DOMParserException (String msg)
	{
		super (msg);
	}
};
