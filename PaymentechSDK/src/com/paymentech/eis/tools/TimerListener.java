package com.paymentech.eis.tools;

import java.util.*;

public interface TimerListener extends EventListener{
    public void actionPerformed (TimerEvent e);
}
