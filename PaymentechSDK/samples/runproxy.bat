@rem
@rem	runproxy.bat
@
@rem    Purpose: Execute an Orbital Java SDK Sample Program using a proxy connection
@rem
@rem    Usage: runproxy <java program name>
@rem
@rem    Example: runproxy AuthSample
@rem

%JAVA_HOME%/jre/bin/java -DPAYMENTECH_HOME=%PAYMENTECH_HOME%  -Dhttp.proxyHost=gatekeeper1 -Dhttp.proxyPort=80 -classpath .;..;%PAYMENTECH_HOME%/lib/PaymentechSDK.jar;%PAYMENTECH_HOME%/lib/jsse.jar;%PAYMENTECH_HOME%/lib/jcert.jar;%PAYMENTECH_HOME%/lib/jnet.jar;%PAYMENTECH_HOME%/lib/commons-httpclient-3.1.jar;%PAYMENTECH_HOME%/lib/commons-codec-1.3.jar;%PAYMENTECH_HOME%/lib/regexp.jar;%PAYMENTECH_HOME%/lib/log4j-1.2.8.jar %1