@rem
@rem	run.bat
@
@rem    Purpose: Execute an Orbital Java SDK Sample Program
@rem
@rem    Usage: run <java program name>
@rem
@rem    Example: run AuthSample
@rem

"%JAVA_HOME%/jre/bin/java" -DPAYMENTECH_HOME=%PAYMENTECH_HOME% -classpath .;%PAYMENTECH_HOME%/lib/PaymentechSDK.jar;%PAYMENTECH_HOME%/lib/commons-logging.jar;%PAYMENTECH_HOME%/lib/jsse.jar;%PAYMENTECH_HOME%/lib/jcert.jar;%PAYMENTECH_HOME%/lib/jnet.jar;%PAYMENTECH_HOME%/lib/commons-httpclient-3.1.jar;%PAYMENTECH_HOME%/lib/commons-codec-1.3.jar;%PAYMENTECH_HOME%/lib/regexp.jar;%PAYMENTECH_HOME%/lib/log4j-1.2.8.jar;%PAYMENTECH_HOME%/lib/sunrsasign.jar %1