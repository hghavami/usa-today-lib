@rem
@rem	build.bat
@
@rem    Purpose: Compiles an Orbital SDK Sample Program
@rem
@rem    Example: build AuthSample.java
@rem             run AuthSample
@rem
@rem    To build all, use "*" as the input argument
@rem
@rem    Example: build *
@rem
"%JAVA_HOME%\bin\javac" -classpath %PAYMENTECH_HOME%/lib/PaymentechSDK.jar %1
