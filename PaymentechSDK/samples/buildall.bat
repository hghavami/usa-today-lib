@rem
@rem	buildAll.bat
@
@rem    Purpose: Compiles All an Orbital SDK Sample Programs
@rem
@rem
%JAVA_HOME%\bin\javac -classpath %PAYMENTECH_HOME%/lib/PaymentechSDK.jar *.java
